<?php

define('BASE_PATH','/assignments/persado/php');

include_once( 'library/Route.php' );
include_once( 'controllers/Api.php' );
include_once( 'controllers/Book.php' );
include_once( 'controllers/Author.php' );
include_once( 'controllers/Publisher.php' );

$api        = new Api();
$book       = new Book();
$author     = new Author();
$publisher  = new Publisher();

Route::add('/', function() {
  echo 'This is an api.';
});

Route::add('/api', function() {
  $GLOBALS['api']->printIndex();
}, 'get');

Route::add('/api/publisher/([0-9]*)', function($publisher_id) {
  $GLOBALS['publisher']->getPublishersBooks($publisher_id);
}, 'get');

Route::add('/api/publisher/([0-9]*)/orderby/(author|book)', function($publisher_id, $order) {
  $GLOBALS['publisher']->getPublishersBooks($publisher_id, $order);
}, 'get');

Route::add('/api/book/([0-9]*)', function($book_id) {
  $GLOBALS['book']->getBook($book_id);
}, 'get');

Route::add('/api/book/([0-9]*)/update', function($book_id) {
  $GLOBALS['book']->update($book_id);
}, 'put');

Route::add('/api/book/([0-9]*)/delete', function($book_id) {
  $GLOBALS['book']->delete($book_id);
}, 'delete');

Route::add('/api/create/book', function() {
  $GLOBALS['book']->create();
}, 'put');

Route::add('/api/create/publisher', function() {
  $GLOBALS['publisher']->create();
}, 'put');

Route::add('/api/create/author', function() {
  $GLOBALS['author']->create();
}, 'put');

Route::add('/api/author/([0-9]*)', function($author_id) {
  $GLOBALS['author']->getAuthor($author_id);
}, 'get');

// 404
Route::pathNotFound(function($path) {
  header('HTTP/1.0 404 Not Found');
  echo 'Page not found';
});

// 405
Route::methodNotAllowed(function($path, $method) {
  header('HTTP/1.0 405 Method Not Allowed');
  echo 'Method not allowed';
});

Route::run(BASE_PATH);