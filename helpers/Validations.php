<?php

/**
* Validations
* 
* This class handles validations and errors
* 
*/
class Validations
{
	public $errors = [	0 => "INVALID_JSON", 
						1 => "GENERAL_ERROR", 
						2 => "INVALID_PARAMETER", 
						3 => "INVALID_REQUIRED_FIELDS",
						4 => "NO_ITEM_FOUND",
						5 => "NO_ROW_UPDATED"
					];
	
	public function isJson($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}

}