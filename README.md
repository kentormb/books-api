# README #

This a simple API for demo purposes with a few endpoints for creating books, authors and publishers. Displaying, updating or deleting books. No authentication is required.


### Available data ###

The root endpoint displays all available data

**URL**: https://marios.com.gr/assignments/persado/php/api

**METHOD**: GET


# Endpoints #

### Publisher ###

Show a list with all visible books from a given publisher. Parameter (publisher id) is the publisher id and should be a number

**URL**: /api/publisher/(publisher id) [ https://marios.com.gr/assignments/persado/php/api/publisher/2 ]

**METHOD**: GET


The list above can be ordered by adding the orderby parameter. The available options are the ones below

* **author**: Alphabetically by author’s last name
* **book**: Books of same author are ordered in descending direction

**URL**: /api/publisher/(publisher id)/orderby/(author|book) [ https://marios.com.gr/assignments/persado/php/api/publisher/2/orderby/book ]

**METHOD**: GET

**RESPONSE**: Json


### Book ###

Show available data of a specific book. 

**URL**: /api/book/(book id) [ https://marios.com.gr/assignments/persado/php/api/book/25 ]

**METHOD**: GET

**RESPONSE**: Json


### Author ###

Show available data of a specific author. 

**URL**: /api/author/(author id) [ https://marios.com.gr/assignments/persado/php/api/author/2 ]

**METHOD**: GET

**RESPONSE**: Json


### Create new entity ###

For all entities Status is 1: visible, 0: not visible.

Create a new publisher with a json dataset added as raw body data. The required fields are name and phone. Phone numbers are considered as a unique value.

**Example Request**

```
{
	"name":"Penguin Books Ltd",
	"phone":"+44 (0)20 7139 3000",
	"address":"One Embassy Gardens, 8 Viaduct Gardens, London, SW11 7BW",
	"status":"1"
}
```

**URL**: /api/create/publisher [ https://marios.com.gr/assignments/persado/php/api/create/publisher ]

**METHOD**: PUT

**RESPONSE**: Json with error code,  error:0 = success



Create a new author with a json dataset added as raw body data. The required fields are first and last name and email. Email is considered as a unique value.

**Example Request**

```
{
	"first_name":"Bill", 
	"last_name":"Gates", 
	"email":"b.gates@microsoft.com", 
	"birthday":"1955-10-28", 
	"status":1
}
```

**URL**: /api/create/author [ https://marios.com.gr/assignments/persado/php/api/create/author ]

**METHOD**: PUT

**RESPONSE**: Json with error code,  error:0 = success



Create a new book with a json dataset added as raw body data. The required fields are title, isbn, publisher and author. Isbn is considered as a unique value.

**Example Request**

The request can be as follows where the publisher or the author are the ids from existing entities. 

```
{	
	"title":"The Longevity Book", 
	"description":"Cameron Diaz follows up her #1 New York Times bestseller, The Body Book, with a personal, practical, and authoritative guide that examines the art and science of growing older and offers concrete steps women can take to create abundant health and resilience as they age. Cameron Diaz wrote The Body Book to help educate young women about how their bodies function, empowering them to make better-informed choices about their health and encouraging them to look beyond the latest health trends to understand their bodies at the cellular level. She interviewed doctors, scientists, nutritionists, and a host of other experts, and shared what she’d learned—and what she wished she’d known twenty years earlier.", 
	"created_at": "2016-01-01", 
	"isbn":"9780008139612", 
	"status":1, 
	"publisher":10, 
	"author":2
}
```

If publisher or author are not previously created then alternatively they can be created during book creation like the following example

```
{	
	"title":"The Longevity Book", 
	"description":"Cameron Diaz follows up her #1 New York Times bestseller, The Body Book, with a personal, practical, and authoritative guide that examines the art and science of growing older and offers concrete steps women can take to create abundant health and resilience as they age. Cameron Diaz wrote The Body Book to help educate young women about how their bodies function, empowering them to make better-informed choices about their health and encouraging them to look beyond the latest health trends to understand their bodies at the cellular level. She interviewed doctors, scientists, nutritionists, and a host of other experts, and shared what she’d learned—and what she wished she’d known twenty years earlier.", 
	"created_at": "2016-01-01", 
	"isbn":"9780008139612", 
	"status":1, 
	"publisher":{
        "name":"Penguin Books Ltd",
        "phone":"+44 (0)20 7139 3000",
        "address":"One Embassy Gardens, 8 Viaduct Gardens, London, SW11 7BW",
        "status":"1"
    }, 
	"author":{
		"first_name":"Cameron", 
		"last_name":"Diaz", 
		"email":"cd72@gmail.com", 
		"birthday":"1972-08-30", 
		"status":1
	}
}
```

**URL**: /api/create/book [ https://marios.com.gr/assignments/persado/php/api/create/book ]

**METHOD**: PUT

**RESPONSE**: Json with error code,  error:0 = success


### Update Book ###

To update a book only the updated fields are needed. 

**Example Request**

```
{	
	"status":0, 
	"author":10,
    "publisher":2
}
```

**URL**: /api/book/(book id)/update [ https://marios.com.gr/assignments/persado/php/api/book/2/update ]

**METHOD**: PUT

**RESPONSE**: Json with error code,  error:0 = success



### Delete Book ###

Delete a book by a given id

**URL**: /api/book/(book id)/delete [ https://marios.com.gr/assignments/persado/php/api/book/2/delete ]

**METHOD**: DELETE

**RESPONSE**: Json with error code,  error:0 = success