<?php 

include 'models/Queries.php';
include 'helpers/Validations.php';

/**
* Api
* 
* This class handles route requests
* Extends validations and error handlers
*/
class Api extends Validations{

	/**
     * @var obj Queries Wrapper
     */
	private $q;				

	public function __construct(){
		$this->q = Queries::getInstance();
	}

	/**
   * 
   * Create json headers
   */
	public function getHeaders(){
		header("Access-Control-Allow-Origin: *");
		header('Content-Type: application/json');
	}

	/**
   * 
   * Print json content 
   */
	public function print($message){
		$this->getHeaders();
		echo json_encode($message);
	}

	/**
   * 
   * Print all available data with their endpoints
   */
	public function printIndex(){
		$this->print($this->q->selectAll());
	}

	/**
   * 
   * Getter for private $q
   */
	public function getQuery(){
		return $this->q;
	}

	/**
   * 
   * Truncate text to a given limit
   *
   * @param  string $text
   * @param  int $limit
   * @return truncated text
   */
	public function truncateText($text, $limit){
		return (strlen($text) > $limit) ? substr($text,0,$limit).'...' : $text;
	}

}