<?php

/**
 * Author
 * 
 * This class handles route requests for author entiry
 * Extends Api 
 */
class Author extends Api
{
	public function __construct(){
		parent::__construct();
	}
	
	/**
   * 
   * Create a new Author
   *
   * @param array $data contains author's fileds
   * @return bool | int author id or false if data is invalid
   */
	public function createAuthor($data){
		return $this->validateAuthor($data) ? $this->getQuery()->insertAuthor($data) : false;
	}

	/**
   * 
   * Validate data and create a new book, author or publisher
   *
   * @param sting $entity the new entity to be created
   * @return prints a json info data depending on the result
   */
	public function create(){
		$error = 0;
		$data = '';
		try{
			$body = file_get_contents( 'php://input', 'r' );
			if($this->isJson($body)){
				$json = json_decode($body, true);
				if($this->validateAuthor($json)){
					$id = $this->createAuthor($json);
					$data = ['author_id' => $id];
				}
				else{
					$error = $this->errors[3];
				}
			}
			else{
				$error = $this->errors[0];
			}
		}
		catch(Exception $e){
			$error = $this->errors[1];
		}
		
		$this->print(['error'=>$error, 'data'=>$data]);
	}


	/**
   * 
   * Print author's info
   *
   * @param int $author_id
   * @return prints a json info data depending on the result
   */
	public function getAuthor($author_id){
		$author = $this->getQuery()->selectAuthor($author_id);
		$author = array_map(array($this, 'mapAuthorItem'), $author);
		$this->print(isset($author[0]) ? $author[0] : ['error' => $this->errors[4]]);
	}



	/**
   * 
   * Beautify date format
   *
   * @param  array $value
   * @return array with formated dates
   */
	public function mapAuthorItem($value){
		$value['birthday'] = date("jS \of F Y", strtotime($value['birthday']));
		return $value;
	}

	public function validateAuthor($author){

		if(!(isset($author['first_name']) && $author['first_name'] != '')){
			return false;
		}
		elseif (!(isset($author['last_name']) && $author['last_name'] != '')) {
			return false;
		}
		elseif (!(isset($author['email']) && $author['email'] != '')) {
			return false;
		}
		
		return true;
	}

	public function validateAuthorUpdate($author){

		if(isset($author['first_name']) && $author['first_name'] == ''){
			return false;
		}
		elseif (isset($author['last_name']) && $author['last_name'] == '') {
			return false;
		}
		elseif (isset($author['email']) && $author['email'] == '') {
			return false;
		}
		
		return true;
	}
}