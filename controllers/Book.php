<?php

include_once( 'controllers/Author.php' );
include_once( 'controllers/Publisher.php' );

/**
 * Book
 * 
 * This class handles route requests for book entiry
 * Extends Api 
 */
class Book extends Api
{
	private $author;

	private $publisher;

	public function __construct(){
		$this->author = new Author();
		$this->publisher = new Publisher();
		parent::__construct();
	}

  /**
   * 
   * Create a new book
   *
   * @param array $data contains book's fileds
   * @return int the created book id
   */
	public function createBook($data){
		
		$publisher_id = '';
		if(isset($data["publisher"])){
			if(is_array($data["publisher"])){
				$publisher_id = $this->publisher->createPublisher($data["publisher"]);
			}
			else{
				$publisher_id = $this->publisher->publisherExists($data["publisher"]) ?: '';
			}
		}

		$data["publisher"] = $publisher_id;

		$book_id = $this->getQuery()->insertBook($data);

		if(isset($data["author"])){
			if(is_array($data["author"])){
				$author_id = $this->author->createAuthor($data["author"]);
				$this->getQuery()->connectBookToAuthor($book_id, $author_id);
				$connectBookToAuthor = $publisher_id != '' ? $this->getQuery()->connectAuthorToPublisher($author_id, $publisher_id) : '';
			}
			else{
				if($author_id = $this->getQuery()->valueExists($data["author"], 'id', 'authors')){
					$this->getQuery()->connectBookToAuthor($book_id, $author_id);
					$connectBookToAuthor = $publisher_id != '' ? $this->getQuery()->connectAuthorToPublisher($author_id, $publisher_id) : '';
				}
			}
		}

		return $book_id;
	}

  /**
   * 
   * Print book's info
   *
   * @param int $book_id
   * @return prints a json info data depending on the result
   */
	public function getBook($book_id){
		$book = $this->getQuery()->selectBook($book_id);
		$book = array_map(array($this, 'mapBookItem'), $book);
		$this->print(isset($book[0]) ? $book[0] : ['error' => $this->errors[4]]);
	}

  /**
   * 
   * Validate data and create a new book, author or publisher
   *
   * @return prints a json info data depending on the result
   */
	public function create(){
		$error = 0;
		$data = '';
		try{
			$body = file_get_contents( 'php://input', 'r' );
			if($this->isJson($body)){
				$json = json_decode($body, true);
				if($this->validateBook($json)){
					$id = $this->createBook($json);
					$data = ['book_id' => $id];
				}
				else{
					$error = $this->errors[3];
				}

			}
			else{
				$error = $this->errors[0];
			}
		}
		catch(Exception $e){
			$error = $this->errors[1];
		}
		
		$this->print(['error'=>$error, 'data'=>$data]);
	}

  /**
   * 
   * Update a book if the data are valid
   *
   * @param int $book_id
   * @return prints a json info data depending on the result
   */
	public function update($book_id){
		$error = 0;
		$data = [];
		try{
			$body = file_get_contents( 'php://input', 'r' );
			if($this->isJson($body)){
				$json = json_decode($body, true);
				if($this->validateBookUpdate($json)){
					
					$publisher_id = '';
					if(isset($json["publisher"])){
						if(is_array($json["publisher"])){
							if($publisher_id = $this->publisher->createPublisher($json["publisher"])){
								$json["publisher"] = $publisher_id;
							}
							else{
								unset($json["publisher"]);
								$data[] = ['publisher' => $this->errors[3]];
							}
						}
						elseif(!$this->getQuery()->valueExists($json["publisher"], 'id', 'publishers')){
							unset($json["publisher"]);
							$data[] = ['publisher' => $this->errors[4]];
						}
					}

					if(isset($json["author"])){
						if(is_array($json["author"])){
							if($author_id = $this->author->createAuthor($json["author"])){
								$this->getQuery()->connectBookToAuthor($book_id, $author_id);
								$connectBookToAuthor = $publisher_id != '' ? $this->getQuery()->connectAuthorToPublisher($author_id, $publisher_id) : '';
								unset($json["author"]);
							}
							else{
								unset($json["author"]);
								$data[] = ['author' => $this->errors[3]];
							}
						}
						elseif(!$author_id = $this->getQuery()->valueExists($json["author"], 'id', 'authors')){
							unset($json["author"]);
							$data[] = ['author' => $this->errors[4]];
						}
					}

					if($output = $this->getQuery()->updateBook($book_id, $json)){
						$data[] = ['book' => 'success'];
					}
					else{
						$error = $this->errors[5];
					}
				}
				else{
					$error = $this->errors[3];
				}
			}
		}
		catch(Exception $e){
			$error = $this->errors[1];
			$data = $e->getMessage();
		}
		
		$this->print(['error'=>$error, 'data'=>$data]);
	}

  /**
   * 
   * Delete a book
   *
   * @param int $book_id
   * @return delete a book and print a json info data depending on the result
   */
	public function delete($book_id){
		if(!$this->getQuery()->valueExists($book_id, 'id', 'books')){
			$this->print(['error'=>$this->errors[4], 'data'=>[]]);
		}
		elseif($this->getQuery()->deleteBook($book_id)){
			$this->print(['error'=>0, 'data'=>[]]);
		}
		else{
			$this->print(['error'=>$this->errors[1], 'data'=>[]]);
		}
	}

  /**
   * 
   * Beautify date format
   *
   * @param  array $value
   * @return array with formated dates
   */
	function mapBookItem($value){
		$value['created_at'] = date("d/m/Y", strtotime($value['created_at']));
		$value['author_birthday'] = date("jS \of F Y", strtotime($value['author_birthday']));
		return $value;
	}


  /**
   * 
   * Validate book data for import
   *
   * @param  array $book
   * @return boolean
   */
	public function validateBook($book){

		if(!(isset($book['title']) && $book['title'] != '')){
			return false;
		}
		elseif (!(isset($book['isbn']) && $book['isbn'] != '')) {
			return false;
		}
		elseif (!(isset($book['publisher']) && $book['publisher'] != '')) {
			return false;
		}
		elseif (!(isset($book['author']) && $book['author'] != '')) {
			return false;
		}
		
		return true;
	}

  /**
   * 
   * Validate book data for update
   *
   * @param  array $book
   * @return boolean
   */
	public function validateBookUpdate($book){

		if(isset($book['title']) && $book['title'] == ''){
			return false;
		}
		elseif (isset($book['isbn']) && $book['isbn'] == '') {
			return false;
		}
		elseif (isset($book['publisher']) && $book['publisher'] == '' ) {
			return false;
		}
		elseif (isset($book['author']) && $book['author'] == '') {
			return false;
		}
		
		return true;
	}
}