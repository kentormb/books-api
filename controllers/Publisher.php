<?php

/**
 * Publisher
 * 
 * This class handles route requests for publisher entiry
 * Extends Api 
 */
class Publisher extends Api
{
	public function __construct(){
		parent::__construct();
	}

	/**
   * 
   * Create a new publisher
   *
   * @param array $data contains publisher's fileds
   * @return bool | int publishers id or false if data is invalid
   */
	public function createPublisher($data){
		return $this->validatePublisher($data) ? $this->getQuery()->insertPublisher($data) : false;
	}

		/**
   * 
   * Validate data and create a new book, author or publisher
   
   * @param sting $entity the new entity to be created
   * @return prints a json info data depending on the result
   */
	public function create(){
		$error = 0;
		$data = '';
		try{
			$body = file_get_contents( 'php://input', 'r' );
			if($this->isJson($body)){
				$json = json_decode($body, true);
				if($this->validatePublisher($json)){
					$id = $this->createPublisher($json);
					$data = ['publisher_id' => $id];
				}
				else{
					$error = $this->errors[3];
				}
			}
			else{
				$error = $this->errors[0];
			}
		}
		catch(Exception $e){
			$error = $this->errors[1];
		}
		
		$this->print(['error'=>$error, 'data'=>$data]);
	}

	/**
   * 
   * Print all books of a publisher 
   *
   * @param int $publisher_id
   * @param string $order order by author name or book position
   * @return prints a json info data depending on the result
   */
	public function getPublishersBooks($publisher_id, $order = null){
		$books = $this->getQuery()->selectPublishersBooks($publisher_id, $order);
		$books = array_map(array($this, 'mapBookDescription'), $books);
		$this->print(isset($books[0]) ? $books : ['error' => $this->errors[4]]);
	}

	/**
   * 
   * Checks if books description is longer than 100 chars and truncate it
   *
   * @param array $value
   * @return array with truncated description
   */
	function mapBookDescription($value){
		$value['description'] =  $this->truncateText($value['description'], 100);
		return $value;
	}

  /**
   * 
   * Validate publisher data for import
   *
   * @param  array $publisher
   * @return boolean
   */
	public function validatePublisher($publisher){

		if(!(isset($publisher['name']) && $publisher['name'] != '')){
			return false;
		}
		elseif(!(isset($publisher['address']) && $publisher['address'] != '')){
			return false;
		}
		
		return true;
	}

  /**
   * 
   * Validate publisher data for update
   *
   * @param  array $publisher
   * @return boolean
   */
	public function validatePublisherUpdate($publisher){

		if(isset($publisher['name']) && $publisher['name'] == ''){
			return false;
		}
		
		return true;
	}

}