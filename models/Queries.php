<?php

include 'library/MysqliDb.php';

/**
* Queries
* 
* This class handles mysql queries
* 
*/
class Queries{

	/**
     * @var obj Database Wrapper
     */
	private $_db;

	/**
     * @var array Database connection arguments
     */
	private $_args;

	/**
     * @var obj instance of self
     */
	private static $instance = null;

	private function __construct(){

		$this->_args = Array (
                'host' => 'localhost',
                'username' => getenv('DB_USER'),
                'password' => getenv('DB_PASS'),
                'db'=> getenv('DB_DATABASE'),
                'port' => 3306,
                'prefix' => '',
                'charset' => 'utf8');

		$this->_db = new MysqliDb($this->_args);

	}

	/**
   * 
   * Implement Singlenton Pattern
   *
   */
	public static function getInstance()
	{
		if (self::$instance == null){
			self::$instance = new Queries();
		}

		return self::$instance;
	}

	/**
   * 
   * Create tables
   *
   */
	public function createTables(){
		$this->createBooksTable();
		$this->createAuthorsTable();
		$this->createPublishersTable();
		$this->createAuthorPublisherTable();
		$this->createBookAuthorTable();
	}

	/**
   * 
   * Create books table
   *
   */
	public function createBooksTable(){
		$this->_db->rawQuery("CREATE TABLE `books` (`id` int(11) NOT NULL, `title` varchar(256) NOT NULL,`description` text NOT NULL, `created_at` datetime NOT NULL DEFAULT current_timestamp(), `isbn` int(11) NOT NULL, `publisherId` int(11) NOT NULL, `status` int(11) NOT NULL DEFAULT 1) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
	}


	/**
   * 
   * Create authors table
   *
   */
	public function createAuthorsTable(){
		$this->_db->rawQuery("CREATE TABLE `authors` (`id` int(11) NOT NULL,`first_name` varchar(64) NOT NULL,`last_name` varchar(64) NOT NULL,`email` varchar(64) NOT NULL,`birthday` date NOT NULL,`status` int(11) NOT NULL DEFAULT 1) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
	}

	/**
   * 
   * Create publishers table
   *
   */
	public function createPublishersTable(){
		$this->_db->rawQuery("CREATE TABLE `publishers` (`id` int(11) NOT NULL,`name` varchar(64) NOT NULL,`phone` varchar(64) NOT NULL,`address` varchar(32) NOT NULL,`status` int(11) NOT NULL DEFAULT 1) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
	}

	/**
   * 
   * Create authors and publishers multi to multi connection table
   *
   */
	public function createAuthorPublisherTable(){
		$this->_db->rawQuery("CREATE TABLE `author_publisher` (`authorId` int(11) NOT NULL, `publisherId` int(11) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
	}

	/**
   * 
   * Create books and authors multi to multi connection table
   *
   */
	public function createBookAuthorTable(){
		$this->_db->rawQuery("CREATE TABLE `book_author` (`bookId` int(11) NOT NULL,`AuthorId` int(11) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
	}


	/**
   * 
   * Show all available data
   *
   */
	public function selectAll(){

		$books = $this->_db->rawQuery('SELECT id, title, CONCAT("/api/book/", id) AS endpoint FROM books');
		$authors = $this->_db->rawQuery('SELECT id, CONCAT(first_name, " ", last_name) AS name, CONCAT("/api/author/", id) AS endpoint FROM authors');
		$publishers = $this->_db->rawQuery('SELECT id, name, CONCAT("/api/publisher/", id) AS endpoint FROM publishers');

		return ["books" => $books, "authors" => $authors, "publishers" => $publishers];
	}

	/**
   * 
   * Insert Book
   *
   * @param array $dataArray book's fields
   * @return int book id
   */
	public function insertBook($dataArray){

		if($id = $this->valueExists($dataArray["isbn"], 'isbn', 'books')){
			return $id;
		}

		$data = Array (
		    "title" 	  	=> $dataArray["title"],
		    "description" 	=> isset($dataArray["description"]) ? $dataArray["description"] : "",
		    "created_at" 	=> isset($dataArray["created_at"]) ? $dataArray["created_at"] : "",
		    "isbn" 			=> $dataArray["isbn"],
		    "publisherId" 	=> $dataArray["publisher"],
		    "status" 		=> isset($dataArray["status"]) ? $dataArray["status"] : "1",
		);
		$id = $this->_db->insert ("books", $data);

		return $id;
	}

	/**
   * 
   * Insert Publisher
   * @param array $dataArray publisher's fields
   * @return int publisher id
   */
	public function insertPublisher($dataArray){
		if($id = $this->valueExists($dataArray["phone"], 'phone', 'publishers')){
			return $id;
		}

		$data = Array (
		    "name" 	  		=> $dataArray["name"],
		    "phone" 		=> $dataArray["phone"],
		    "address" 		=> isset($dataArray["address"]) ? $dataArray["address"] : "",
		    "status" 		=> isset($dataArray["status"]) ? $dataArray["status"] : "1",
		);
		$id = $this->_db->insert ("publishers", $data);

		return $id;
	}

	/**
   * 
   * Insert Author
   *
   * @param array $dataArray author's fields
   * @return int author id
   */
	public function insertAuthor($dataArray){

		if($id = $this->valueExists($dataArray["email"], 'email', 'authors')){
			return $id;
		}

		$data = Array (
		    "first_name" 	=> $dataArray["first_name"],
		    "last_name" 	=> $dataArray["last_name"],
		    "email" 		=> $dataArray["email"],
		    "birthday" 		=> isset($dataArray["birthday"]) ? $dataArray["birthday"] : "",
		    "status" 		=> isset($dataArray["status"]) ? $dataArray["status"] : "1",
		);
		$id = $this->_db->insert ("authors", $data);

		return $id;
	}

	/**
   * 
   * Connect Author to publisher
   *
   * @param int $authorId author id
   * @param int $publisherId publisher id
   * @return boolean true if row created successfuly
   */
	public function connectAuthorToPublisher($authorId, $publisherId){

		if($this->valueExists($authorId, 'id', 'authors') && $this->valueExists($publisherId, 'id', 'publishers')){
			$data = Array (
			    "authorId" 		=> $authorId,
			    "publisherId" 	=> $publisherId
			);
			return $this->_db->insert ("author_publisher", $data);
		}

		return false;
	}

	/**
   * 
   * Connect Author to book
   *
   * @param int $authorId author id
   * @param int $bookId book id
   * @return boolean true if row created successfuly
   */
	public function connectBookToAuthor($bookId, $authorId){

		if($this->valueExists($authorId, 'id', 'authors') && $this->valueExists($bookId, 'id', 'books')){
			$data = Array (
			    "authorId" 		=> $authorId,
			    "bookId" 		=> $bookId
			);
			return $this->_db->insert ("book_author", $data);
		}

		return false;
	}

	/**
   * 
   * Update book
   *
   * @param int $id book id
   * @param array $dataArray book's fields
   * @return boolean true if row updated successfuly
   */
	public function updateBook($id, $dataArray){

		if(!$this->valueExists($id, 'id', 'books')){
			return false;
		}
		else{
			$data = Array ();

			$author_updated = false;

			if(isset($dataArray["title"])){
				$data['title'] = $dataArray["title"];
			}
			if(isset($dataArray["description"])){
				$data['description'] = $dataArray["description"];
			}
			if(isset($dataArray["created_at"])){
				$data['created_at'] = $dataArray["created_at"];
			}
			if(isset($dataArray["isbn"])){
				$data['isbn'] = $dataArray["isbn"];
			}
			if(isset($dataArray["publisher"])){
				$data['publisherId'] = $dataArray["publisher"];
			}
			if(isset($dataArray["status"])){
				$data['status'] = $dataArray["status"];
			}
			if(isset($dataArray["author"])){
				$this->_db->where ('bookId', $id);
				if($this->_db->update ('book_author', ['authorId'=>$dataArray["author"]])){
					$author_updated = $this->_db->count;
				}
			}

			$this->_db->where ('id', $id);

			if ($this->_db->update ('books', $data) || $author_updated)
			    return $author_updated ? 1 : $this->_db->count;
			else
			    return 0; //$this->_db->getLastError();
		}
	}

	/**
   * 
   * Delete book
   *
   * @param int $id book id
   * @return boolean true if row deleted successfuly
   */
	public function deleteBook($id){
		$this->_db->where('id', $id);
		if($this->_db->delete('books')){
			$this->_db->where('bookId', $id);
			$this->_db->delete('book_author');
			return true;
		}
		else{
			return false;
		}
	}

	/**
   * 
   * Select all books af a publisher in specific order if reqired
   *
   * @param int $publisherId
   * @param string $order can be author or book
   * @return array 
   */
	public function selectPublishersBooks($publisherId, $order = null){

		$sql = 'SELECT title, description, isbn, CONCAT(last_name, " ", first_name) AS author FROM `books` LEFT JOIN book_author ON books.id = book_author.bookId LEFT JOIN `authors` ON `authors`.id = book_author.authorId WHERE publisherId = ' . $this->_db->escape($publisherId);

		if($order == "author"){
			$sql .= ' ORDER BY last_name';
		}
		elseif($order == "book"){
			$sql .= ' ORDER BY last_name, books.id desc';
		}
		
		return $this->_db->rawQuery ($sql);
	}

	/**
   * 
   * Select book's data
   *
   * @param int $id book id
   * @return array
   */
	public function selectBook($id){

		$sql = 'SELECT title, description, isbn, created_at, CONCAT(last_name, " ", first_name) AS author, authors.email as author_email, authors.birthday as author_birthday, publishers.name as publisher_name, publishers.address as publisher_address FROM `books` LEFT JOIN book_author ON books.id = book_author.bookId LEFT JOIN `authors` ON `authors`.id = book_author.authorId LEFT JOIN publishers on books.publisherId = publishers.id WHERE books.id = ' . $this->_db->escape($id);

		return $this->_db->rawQuery ($sql);
	}

	/**
   * 
   * Select author's data
   *
   * @param int $id author id
   * @return array
   */
	public function selectAuthor($id){

		$sql = 'SELECT CONCAT(last_name, " ", first_name) AS name, email, birthday FROM authors WHERE id = ' . $this->_db->escape($id);
		return $this->_db->rawQuery ($sql);
	}

	/**
   * 
   * Check if a given row exists in array
   *
   * @param string $value the column value to be checked if exists
   * @param int $col column name
   * @param int $table table name
   * @return boolean
   */
	public function valueExists($value, $col, $table){

		$this->_db->where ( $col, $value );
		$id = $this->_db->getValue ($table, "id");
		return $id ?: false;
	}

}


